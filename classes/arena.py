from classes.person import Person
from classes.attack import Attack
from classes.fight import Fight 

class Arena():

	def __init__(self):
		#create 2 players
		self.user = Person('User',True)
		self.comp = Person('Computer',False)
		self.attack = Attack()
		self.fight = Fight()

	def start(self):
		#output who fight 
		self.fight.startFightInfo(self.user.getName(), self.comp.getName())
	
		while self.comp.getHealth() > 0 and self.user.getHealth()>0:		
			#lot who walks
			#1 user walks
			#0 computer walks
			isStep = self.fight.randStepsOrListAtack(self.comp.getIsUser(),self.user.getIsUser());
			print()
			#we check who walks
			if isStep:
				#roll a die from 1 to 3 to complete the move
				dice =  self.fight.randStepsOrListAtack(1,3)
				damage = self.attacking(dice, self.user.getName())
				if dice != 3: 
					self.comp.setHealth(dice, damage)
				else:
					self.user.setHealth(dice, damage)
			else:
		 	#do a check to see if not less than 35 health on the computer
				if self.comp.getHealth()>35:
	 			#roll a die from 1 to 3 to complete the move
					dice =  self.fight.randStepsOrListAtack(1,3)
					damage = self.attacking(dice, self.comp.getName())
					if dice != 3: 
						self.user.setHealth(dice, damage)
					else:
						self.comp.setHealth(dice, damage)	
				else:
				#we cheapen more chance for treatment for the computer
					dice =  self.fight.randStepsOrListAtack(2,3)
					damage = self.attacking(dice, self.comp.getName())
					if dice != 3: 
						self.user.setHealth(dice, damage)
					else:
						self.comp.setHealth(dice, damage)

			print(f"{self.user.getName()}:{self.user.getHealth()} hp vs {self.comp.getName()}:{self.comp.getHealth()} hp")		
		
		if(self.comp.getHealth()!=0):
			print(f'{self.comp.getName()} Win!!')
		else:
			print(f'{self.user.getName()} Win!!')	
				
				
	def attacking(self, dice, name):
		
		if dice == 1:			
			return self.attack.normalAttack(name)
		if dice == 2:
			return self.attack.criticalAttack(name)
		if dice == 3:
			return self.attack.heal(name)	



				
		


		
	

	

