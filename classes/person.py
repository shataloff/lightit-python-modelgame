class Person():
	
	#add constructor
	def __init__(self, name, isUser):
		self.__name = name
		self.__isUser = isUser
		#players' lives
		self.__health = 100

	def getName(self):
		return self.__name
	
	def getIsUser(self):
		return self.__isUser

	def getHealth(self):
		return self.__health
	
	def setHealth(self, dice,value):
		if(dice!=3):
			self.__health-=value
			if(self.__health<0):
				self.__health=0
		else:
			self.__health+=value
			#if the treatment produces more than 100 hp, leave 100
			if(self.__health > 100):
				self.__health=100
			

