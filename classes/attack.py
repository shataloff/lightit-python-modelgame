from classes.fight import Fight
import random
 
class Attack():
	def __init__(self):
		self.fight = Fight()
	#random damage Attack and message 	
	def normalAttack(self, name):
		damage = self.fight.randStepsOrListAtack(18, 25)
		print(f'Attack from {name} normalAttack, damage:{damage}')
		return damage

	def criticalAttack(self, name):
		damage = self.fight.randStepsOrListAtack(10, 35)
		print(f'Attack from {name} criticalAttack, damage:{damage}')
		return damage

	def heal(self, name):
		damage = self.fight.randStepsOrListAtack(18, 25)
		print(f'Heal from {name} :{damage}')
		return damage		
